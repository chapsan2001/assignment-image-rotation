#ifndef PICTURE_TRANSFORMATION_PICTURE_H_
#define PICTURE_TRANSFORMATION_PICTURE_H_

#include <stdint.h>

struct Pixel {
  uint8_t blue;
  uint8_t green;
  uint8_t red;
};

struct Picture {
  uint64_t height;
  uint64_t width;
  struct Pixel* data;
};

#endif //PICTURE_TRANSFORMATION_PICTURE_H_
