#ifndef PICTURE_TRANSFORMATION_TRANSFORMATIONS_ROTATION_LEFT90_H_
#define PICTURE_TRANSFORMATION_TRANSFORMATIONS_ROTATION_LEFT90_H_

#include <stdlib.h>
#include "../picture.h"

static uint64_t getNewPicturePixelAddress(const struct Picture* const source, uint64_t x, uint64_t y);
struct Picture rotateLeft90(struct Picture const source);

#endif //PICTURE_TRANSFORMATION_TRANSFORMATIONS_ROTATION_LEFT90_H_
