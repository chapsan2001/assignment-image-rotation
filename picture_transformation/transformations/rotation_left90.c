#include "rotation_left90.h"

static uint64_t getNewPicturePixelAddress(const struct Picture* const source, uint64_t x, uint64_t y) {
    return x * source->height + source->height - y - 1;
}

struct Picture rotateLeft90(struct Picture const source) {
  struct Picture newPicture;

  newPicture.height = source.width;
  newPicture.width = source.height;
  newPicture.data = malloc(source.width * source.height * sizeof(struct Pixel));

  for (size_t y = 0; y < source.height; ++y) {
    for (size_t x = 0; x < source.width; ++x) {
      *(newPicture.data + getNewPicturePixelAddress(&source, x, y)) = *(source.data + y * source.width + x);
    }
  }

  return newPicture;
}