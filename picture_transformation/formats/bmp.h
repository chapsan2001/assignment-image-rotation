#ifndef PICTURE_TRANSFORMATION_FORMATS_BMP_H_
#define PICTURE_TRANSFORMATION_FORMATS_BMP_H_

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <memory.h>
#include "../picture.h"

#define COLOR_DEPTH 24
#define PADDING_SIZE 4
#define BMP_TYPE 0x4d42

#pragma pack(push, 1)
struct BmpHeader {
  uint16_t bfType;
  uint32_t bfileSize;
  uint32_t bfReserved;
  uint32_t bOffBits;
  uint32_t biSize;
  uint32_t biWidth;
  uint32_t biHeight;
  uint16_t biPlanes;
  uint16_t biBitCount;
  uint32_t biCompression;
  uint32_t biSizeImage;
  uint32_t biXPelsPerMeter;
  uint32_t biYPelsPerMeter;
  uint32_t biClrUsed;
  uint32_t biClrImportant;
};
#pragma pack(pop)

uint32_t getPadding(const uint64_t width);

enum ReadStatus {
  READ_OK = 0,
  READ_INVALID_BITS = 1,
  READ_INVALID_HEADER = 2,
  READ_INVALID_SIGNATURE = 3,
  READ_UNSUPPORTED_DEPTH = 4,
};

enum ReadStatus fromBmp(FILE* in, struct Picture* pic);
enum ReadStatus checkHeader(FILE* in, struct BmpHeader *header);

enum WriteStatus {
  WRITE_OK = 0,
  WRITE_ERROR = 1,
  WRITE_INVALID_BITS = 2,
};

enum WriteStatus toBmp(FILE* out, struct Picture const* pic);

#endif //PICTURE_TRANSFORMATION_FORMATS_BMP_H_
