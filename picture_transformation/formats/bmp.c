#include "bmp.h"

struct BmpHeader createBmpHeader(struct Picture const pic) {
    struct BmpHeader header = {0};

    uint32_t headerSize = sizeof(struct BmpHeader);
    uint32_t pixelSize = sizeof(struct Pixel);

    header.bfType = BMP_TYPE;
    header.biBitCount = COLOR_DEPTH;
    header.biHeight = pic.height;
    header.biWidth = pic.width;
	header.bOffBits = headerSize;
    header.bfileSize = headerSize + (pixelSize * pic.width + pic.width % 4) * pic.height;
    header.biSizeImage = pic.width * pic.height * pixelSize;
    header.biSize = 40;
    header.biPlanes = 1;
    header.bfReserved = 0;
    header.biCompression = 0;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    return header;
}

uint32_t getPadding(const uint64_t picWidth) {
  size_t widthSize = picWidth * sizeof(struct Pixel);
  if (widthSize % PADDING_SIZE == 0) return 0;
  return PADDING_SIZE - (widthSize % PADDING_SIZE);
}

enum ReadStatus checkHeader(FILE* in, struct BmpHeader *header) {
    const uint32_t headerSize = sizeof(struct BmpHeader);

    if (fread(header, headerSize, 1, in) < 1) {
        if (feof(in)) return READ_INVALID_BITS;
        return READ_INVALID_HEADER;
    }
    if (fseek(in, header->bOffBits, SEEK_SET) != 0) return READ_INVALID_BITS;
    if (header->biBitCount != COLOR_DEPTH) return READ_UNSUPPORTED_DEPTH;
    if (header->bfType != BMP_TYPE) return READ_INVALID_SIGNATURE;
    return READ_OK;
}

enum ReadStatus fromBmp(FILE* in, struct Picture* pic) {
  struct BmpHeader header = {0};
  const uint32_t pixelSize = sizeof(struct Pixel);
  enum ReadStatus readStatus = checkHeader(in, &header);

  pic->height = header.biHeight;
  pic->width = header.biWidth;
  pic->data = malloc(pic->width * pic->height * pixelSize);

  uint32_t padding = getPadding(pic->width);

  for (size_t y = 0; y < pic->height; ++y) {
    if (fread(pic->data + y * pic->width, pixelSize, pic->width, in) < pixelSize) readStatus = READ_INVALID_BITS;
    if (fseek(in, padding, SEEK_CUR) != 0) readStatus = READ_INVALID_BITS;
  }

  if (fseek(in, 0, SEEK_SET) != 0) readStatus = READ_INVALID_BITS;
  if (readStatus != READ_OK) {
      free(pic->data);
  }
  return readStatus;
}

enum WriteStatus toBmp(FILE* out, struct Picture const* pic) {
  struct BmpHeader header = createBmpHeader(*pic);

  uint32_t headerSize = sizeof(struct BmpHeader);
  uint32_t pixelSize = sizeof(struct Pixel);

  if (fwrite(&header, headerSize, 1, out) < 1) return WRITE_ERROR;

  char paddingBytes[3] = {0};
  uint32_t padding = getPadding(pic->width);

  for (size_t y = 0; y < pic->height; ++y) {
    if (fwrite(pic->data + y * pic->width, pixelSize, pic->width, out) != pic->width) return WRITE_ERROR;
    if (fwrite(paddingBytes, padding, 1, out) != 1 && padding != 0) return WRITE_ERROR;
    if (fflush(out) != 0) return WRITE_ERROR;
  }

  if (fseek(out, 0, SEEK_SET) != 0) return WRITE_ERROR;
  return WRITE_OK;
}
