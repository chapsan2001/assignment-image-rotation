#ifndef PICTURE_TRANSFORMATION_UTILS_FILE_H_
#define PICTURE_TRANSFORMATION_UTILS_FILE_H_

#include <stdio.h>

enum OpenStatus {
  OPEN_OK = 0,
  OPEN_ERROR = 1,
};

enum OpenStatus openFile(FILE** file, const char* path);

enum CloseStatus {
  CLOSE_OK = 0,
  CLOSE_ERROR = 1,
};

enum CloseStatus closeFile(FILE** file);

#endif //PICTURE_TRANSFORMATION_UTILS_FILE_H_
