#include "message.h"

void printMessage(const char* message, ...) {
  va_list args;
  va_start(args, message);
  vfprintf(stdout, message, args);
  fprintf(stdout, "\n");
  va_end(args);
}

void throwError(const char* message, ...) {
  va_list args;
  va_start(args, message);
  vfprintf(stderr, message, args);
  fprintf(stderr, "\n");
  va_end(args);
  exit(1);
}

const char* openStatusMessages[] = {
    "File was opened successfully.",
    "File opening error - check file existence and its access rights.",
};

const char* unknownOpeningError = "Unknown file opening error.";

const char* openStatusToMessage(const enum OpenStatus openStatus) {
    if (openStatus < 0 || openStatus > 1) return unknownOpeningError;
    return openStatusMessages[openStatus];
}

const char* readStatusMessages[] = {
    "File was read successfully.",
    "Invalid bits error.",
    "Invalid header error.",
    "Invalid signature error.",
    "Unsupported color depth error - only 24 bits supported.",
};

const char* unknownReadingError = "Unknown file reading error.";

const char* readStatusToMessage(const enum ReadStatus readStatus) {
    if (readStatus < 0 || readStatus > 4) return unknownReadingError;
    return readStatusMessages[readStatus];
}

const char* writeStatusMessages[] = {
    "File was written successfully.",
    "File writing error.",
    "Invalid bits error.",
};

const char* unknownWritingError = "Unknown file writing error.";

const char* writeStatusToMessage(const enum WriteStatus writeStatus) {
    if (writeStatus < 0 || writeStatus > 2) return unknownWritingError;
    return writeStatusMessages[writeStatus];
}

const char* closeStatusMessages[] = {
    "File was closed successfully.",
    "File closing error.",
};

const char* unknownClosingError = "Unknown file closing error.";

const char* closeStatusToMessage(const enum CloseStatus closeStatus) {
    if (closeStatus < 0 || closeStatus > 1) return unknownClosingError;
    return closeStatusMessages[closeStatus];
}