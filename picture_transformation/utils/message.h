#ifndef PICTURE_TRANSFORMATION_UTILS_MESSAGE_H_
#define PICTURE_TRANSFORMATION_UTILS_MESSAGE_H_

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include "file.h"
#include "../formats/bmp.h"

void printMessage(const char* message, ...);
void throwError(const char* message, ...);

const char* openStatusToMessage(const enum OpenStatus openStatus);
const char* readStatusToMessage(const enum ReadStatus readStatus);
const char* writeStatusToMessage(const enum WriteStatus writeStatus);
const char* closeStatusToMessage(const enum CloseStatus closeStatus);

#endif //PICTURE_TRANSFORMATION_UTILS_MESSAGE_H_
