#include "formats/bmp.h"
#include "transformations/rotation_left90.h"
#include "utils/file.h"
#include "utils/message.h"

void showUsage() {
  printMessage("Usage: picture_transformation <file path>");
}

int main(int argc, char** argv) {
  if (argc != 2 || argv[1] == NULL) showUsage();
  if (argc > 2) throwError("Too many arguments.");
  if (argc < 2) throwError("Not enough arguments.");

  struct Picture pic;
  FILE* pictureFile = NULL;

  enum OpenStatus openStatus = openFile(&pictureFile, argv[1]);
  if (openStatus != OPEN_OK)
    throwError("An error occured while trying to open the file (path: %s, code: %d, message: %s).",
          argv[1], openStatus, openStatusToMessage(openStatus));

  enum ReadStatus readStatus = fromBmp(pictureFile, &pic);
  if (readStatus != READ_OK)
    throwError("An error occured while trying to read the file (code: %d, message: %s).",
          readStatus, readStatusToMessage(readStatus));

  struct Picture newPicture = rotateLeft90(pic);

  enum WriteStatus writeStatus = toBmp(pictureFile, &newPicture);
  if (writeStatus != WRITE_OK)
    throwError("An error occured while trying to write the file (code: %d, message: %s).",
          writeStatus, writeStatusToMessage(writeStatus));

  enum CloseStatus closeStatus = closeFile(&pictureFile);
  if (closeStatus != CLOSE_OK)
    throwError("An error occured while trying to close the file (code: %d, message: %s).",
          closeStatus, closeStatusToMessage(closeStatus));

  printMessage("The picture was transformed successfully.");
  return 0;
}
